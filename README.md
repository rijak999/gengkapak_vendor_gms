# vendor_gms

## Basic usage

- If you just wanna use this repository, then you just need to include `products/gms.mk`
- This is as simple as: `$(call inherit-product-if-exists, vendor/gms/products/gms.mk)`

GMS mandatory core packages:
```
AndroidAutoStub
AndroidPlatformServices
ConfigUpdater
GoogleExtShared
GoogleFeedback
GoogleLocationHistory
GoogleOneTimeInitializer
GooglePackageInstaller
GooglePartnerSetup
GooglePrintRecommendationService
GoogleRestore
GoogleServicesFramework
GoogleCalendarSyncAdapter
GoogleContactsSyncAdapter
SpeechServicesByGoogle
GmsCore
Phonesky
SetupWizard
WebViewGoogle
Wellbeing
```

GMS mandatory application packages
```
Chrome
Gmail2
Maps
Photos
Velvet
Videos
```

GMS optional application packages
```
CalendarGoogle
DeskClockGoogle
LatinImeGoogle
TagGoogle
talkback
Keep
CalculatorGoogle
FilesGoogle
```

Additionally the following Pixel specific apps are included:
```
MarkupGoogle
DevicePersonalizationPrebuiltPixel2020
NexusLauncherRelease
PixelSetupWizard
DocumentsUIGoogle
```
